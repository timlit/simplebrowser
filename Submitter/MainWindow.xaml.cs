﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using AE.Net.Mail;
using SimpleBrowser;
using System.Net;
//
using MailKit.Net.Imap;
using MailKit.Search;
using MailKit;
using MimeKit;
using System.ComponentModel;
//



namespace Submitter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BackgroundWorker bgw = new BackgroundWorker { WorkerReportsProgress = true };
        public MainWindow()
        {
            InitializeComponent();
            bgw.WorkerSupportsCancellation = true;
            bgw.WorkerReportsProgress = true;
            bgw.RunWorkerCompleted += bgw_Completed;
            bgw.ProgressChanged += bgw_ProgressChanged;
        }


        /*
           string url = @"https://gigiena.pro/auth/lencamp/login.php";

           IDictionary<string, string[]> bulk = new Dictionary<string, string[]>();
           List<string[]> bulk2 = new List<string[]>();
           string[] item = { "3be950d2", "EUaR##NV8Wpq", "id", "ln", "fn"};

           bulk.Add("3be950d2", item);

           bulk = browse(url, bulk);
        */



        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //Source = "images\help.bmp"
            BgwHook(sender);
            //bgw.DoWork += bgw_DoWork(sender, e, 100);
        }


        private void BgwHook(object sender)
        {
            IDictionary<string, string> args = new Dictionary<string, string>();
            //int pbarmax = 10000;
            //pbargo(pbar, 1, 3);

            
            //bgw.ProgressChanged += (obj, e) => bgw_ProgressChanged(pbar, pbarmax);
            //bgw.DoWork += null; // += BgwRouter;
            bgw.DoWork += (obj, e) => BgwRouter(sender, e, obj);
            bgw.RunWorkerAsync(); // argument: pbarmax


        }


        //object obj, DoWorkEventArgs e, int pbarmax, IDictionary<string, string> args
        private void BgwRouter(object sender, DoWorkEventArgs e, object obj)
        {

            string title = SenderName(sender);
            //MessageBox.Show("*" + title + "*");

            if (title == "test")
            {
                //bgw_DoWork(obj, e, pbarmax);
                simple_forloop(obj, e);
            }

           

        }//


        private string SenderName(object sender)
        {
            string[] pctitle = sender.ToString().ToLower().Split(':');
            string title = pctitle[1].Trim();
            return title;
        }


        private void pbargo(ProgressBar pbar, int min, int max = 0)
        {//tk
            pbar.Value = 0;
            pbar.Minimum = min;
            pbar.Maximum = max;
            //pbar.Step = 1;
        }


        //BACKGROUNDWORKER

        void simple_forloop(object sender, DoWorkEventArgs args) //, int pbarmax
        {
            //https://stackoverflow.com/questions/4807152/sending-arguments-to-background-worker
            //int pbarmax = (int)e.Argument; // bgw.RunWorkerAsync(argument: max);
            //https://stackoverflow.com/questions/8761060/how-to-refresh-progressbar-accurately
            // do your long running operation here
            int pbarmax = 3;
            //BackgroundWorker bgw = sender as BackgroundWorker;
            bgw.ReportProgress(-1, pbarmax);
            //bgw.ProgressChanged += (obj, e) => bgw_ProgressChanged(pbar, pbarmax);
            
            //pbargo(pbar, 1, pbarmax);
            //pbar.DataContext
            //pbar.Maximum = pbarmax;

            for (int idx = 1; idx <= pbarmax; idx++)
            {

                //MessageBox.Show(" ");
                

                if (bgw.CancellationPending)
                {
                    args.Cancel = true;
                    break;
                }
                else
                {
                    // when using PerformStep() the percentProgress arg is redundant
                    //((BackgroundWorker)sender).ReportProgress(idx*10);
                    bgw.ReportProgress(100*idx /pbarmax);
                    //Thread.Sleep(10);
                }
            }
            //MessageBox.Show("All Done!");

            // and to transport a result back to the main thread
            double result = 0.1 * pbarmax;
            args.Result = result;


        }//

        //object sender, ProgressChangedEventArgs e
        void bgw_ProgressChanged(ProgressBar tpbar, int pbarmax)
        {
            //if (tpbar.Maximum == 0)

            tpbar.Maximum = pbarmax;
            tpbar.Value += 1;
           
        }//


        void bgw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == -1)
            {
                pbar.Maximum = (Int32)(e.UserState);
            }
            else if (e.ProgressPercentage == 100)
            {
                //report();
            }
            else
            { pbar.Value += 1; }

        }//


        // the Completed handler should follow this pattern 
        // for Error and (optionally) Cancellation handling
        void bgw_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            // check error, check cancel, then use result
            if (e.Error != null)
            {
                // handle the error
                MessageBox.Show(e.ToString());
            }
            else if (e.Cancelled)
            {
                // handle cancellation
                MessageBox.Show(e.ToString());
            }
            else
            {
                //double result = (double)e.Result;
                // use it on the UI thread
                pbar.Value = 0;
                MessageBox.Show("All Done! " + sender.ToString());
            }
                
            // general cleanup code, runs when there was an error or not.
        }
        //BACKGROUNDWORKER



        //
        //private void samplego(string url, List<string[]> bulk)
        private IDictionary<string, string[]> browse(string url, IDictionary<string, string[]> bulk)
        {
            //https://github.com/SimpleBrowserDotNet/SimpleBrowser/wiki/Tutorial

            //string url = "";


            //Регистрация на сайте gigiena.pro
            //info@gigiena.pro
            IDictionary<string, string[]> result = new Dictionary<string, string[]>();
            string report = "";
            //foreach(string[] bitem in bulk) {
            foreach (var row in bulk)
            {

            IDictionary<string, string> data = new Dictionary<string, string>();
           
            data["logout"] = "";
            

            // start
            //url = "https://gigiena.pro/auth/lencamp/login.php";

            //Assign NEW browser instance to EACH new login procedure!
            SimpleBrowser.Browser b = new SimpleBrowser.Browser();


            b.Navigate(url);
            Thread.Sleep(10);
            b.Navigate(url);


           

                var bitem = row.Value;
                //string login = "3be950d2";
                //string password = "EUaR##NV8Wpq";

                string login    = bitem[0];
                string password = bitem[1];

                //login
                var item = b.Find(ElementType.TextField, FindBy.Name, "username");
            item.Value = login;

            item = b.Find(ElementType.TextField, FindBy.Name, "password");
            item.Value = password;

            //
            var button = b.Find(ElementType.Button, FindBy.Id, "loginbtn");
            button.Click();


            //fwrite("login.html", b.CurrentHtml);


            //starter
            //string stopper = "label_1_1";
            var starter = b.Find("a", FindBy.Id, "mlabel_1_1");


            //find exit url
            var links = b.Find("a", new object { });

            foreach (var link in links)
            {


                string tlink = "";

                if (link.GetAttribute("href") != null)
                    tlink = link.GetAttribute("href");

                if (tlink.Contains("logout.php?sesskey="))
                {
                    if (data["logout"] == "")
                        data["logout"] = tlink;

                    //break;
                }

            }//


            if (data["logout"] == "")
            {
                MessageBox.Show("Ошибка выхода из системы", "Не найдена ссылки сессии! "
                    + login
                    );
                return result;

            }


            // Click Thru Answers:
            //1.find
            //2.interact
            List<string> actions = new List<string>();
            links = b.Find("div", new object { });

            foreach (var link in links)
            {


                string tlink = "";

                if (link.GetAttribute("onclick") != null
                    && link.GetAttribute("class") != null
                    && link.GetAttribute("class") == "progressBarCell lastProgressBarCell"

                    )
                {//
                    tlink = link.GetAttribute("onclick");

                    //document.location = 'https://gigiena.pro/mod/quiz/view.php?id=364';
                    tlink = tlink.Replace("document.location='", "");
                    tlink = tlink.Replace("';", "");
                    actions.Add(tlink);

                    //Console.WriteLine(tlink);

                    report += tlink + "\n\r";
                }//

            }//foreach





            foreach (string a in actions)
            {

                //b.Navigate("https://gigiena.pro/");
                b.Navigate(a);
            }
            
            
            //END of clicking



                //!!!!!!!!!!!!!!!!!!!!!!!!!




                // GET Profile Data

                string[] trimmer = {

                    "ТИПОГРАФСКИЙ НОМЕР ЛМК",                 
                    "ID (ВАШ ИДЕНТИФИКАЦИОННЫЙ НОМЕР)",
                    "Дата рождения"
                 


                };

                string[] months = {

                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"


                };



                b.Navigate("https://gigiena.pro/user/profile.php");

                links = b.Find("li", new object { });
                report = "";
                int i = 0;
                /* */
                string[] profdata = new string[6];
               

                int pd = 0;
                foreach (var link in links)
                {
                    //profdata = new string[5];


                    
                    string tlink = "";

                    if ( link.GetAttribute("class") != null
                        && link.GetAttribute("class") == "contentnode"

                        )
                    {//
                        tlink = link.Value;
                        //if (tlink.Contains("Город")) break;
                        if (i==3) break;


                        foreach (string trim in trimmer)
                        {
                            tlink = tlink.Replace(trim, "");
                        }


                        int mt = 1;
                        foreach(string m in months)
                        {
                            tlink = tlink.Replace(m, "." + mt + "." );

                            mt++;
                        }

                        //char[] charsToTrim = { '*', ' ', '\'' };
                        //tlink = tlink.Trim(charsToTrim);


                        tlink = tlink.Replace(" ", "");

                        //expect 3 elements
                        profdata[pd] = tlink;
                        pd++;



                        

                        report += 
                            //" (" + i.ToString() + ") " + 
                            tlink + "\n\r";
                        i++;
                    }//

                    result[row.Key] = profdata;
                }//foreach

                //var item = b.Find(ElementType.TextField, FindBy.Class, "username");

                profdata[3] = bitem[3];
                profdata[4] = bitem[4];





                //=======================
                //=======================
                //=======================


                b.Navigate(data["logout"]);
                /*
                Thread.Sleep(50);
                b.Navigate(data["logout"]);
                Thread.Sleep(50);
                b.Navigate(data["logout"]);
                */



                /*
                fwrite("logout.html", b.CurrentHtml);
                Thread.Sleep(50);
                */

        }//foreach in bulk



            //fwrite("report.html", report);



            return result;

        }




        static string GetApplicationRoot()
        {//TK

            //string exePath = GetApplicationRoot() + @"\";


            var exePath = System.IO.Path.GetDirectoryName(System.Reflection
                              .Assembly.GetExecutingAssembly().CodeBase);
            Regex appPathMatcher = new Regex(@"(?<!fil)[A-Za-z]:\\+[\S\s]*?(?=\\+bin)");
            var appRoot = appPathMatcher.Match(exePath).Value;

            //
            exePath = exePath.Replace("file:\\", "");

            /*
            appRoot = appRoot.Replace("file:\\", "");
            appRoot = appRoot.Replace("file:/", "");//linux
            */
            return exePath;
        }


        private string fread(string filepath)
        {
            //string result = "";
            string content = File.ReadAllText(filepath);

            return content;

        }

        private void sample_interact()
        {

            //------

            SimpleBrowser.Browser b = new SimpleBrowser.Browser();
            b.Navigate("http://www.tizag.com/phpT/examples/formexample.php");

            // Find a text input
            var firstName = b.Find(ElementType.TextField, FindBy.Name, "Fname");
            firstName.Value = "Tim";

            // Note: The HTML form had a maxlength attribute limiting the text to 12 characters. Therefore the value of the text input varies from what was assigned.
            Console.WriteLine(firstName.Value);
            // Michelangeli

            // Find a radio button
            var gender = b.Find(ElementType.RadioButton, FindBy.Value, "Male");
            gender.Checked = true;

            // This will also work to set the selected state of a radio button.
            gender.Click();

            // Find a check box
            var food = b.Find("input", new { name = "food[]", value = "Pizza" });

            // This will work to toggle the state of a check box ...
            food.Click();

            // ... but this will set it to a known value.
            food.Checked = true;

            // Find a textarea (note that a text input and textarea are both of type ElementType.TextField)
            var quote = b.Find(ElementType.TextField, FindBy.Name, "quote");
            quote.Value = "I love it when a plan comes together.";

            // Find a select (drop-down box)
            var education = b.Find(ElementType.SelectBox, FindBy.Name, "education");
            education.Value = "College";

            // Find a select (drop-down box)
            var time = b.Find(ElementType.SelectBox, FindBy.Name, "TofD");
            time.Value = "Day";


            //---



            //var button = b.Find("submit");
            //button.SubmitForm();
            var button = b.Find(ElementType.Button, FindBy.Name, "submit");
            button.Click();






            fwrite("result.html", b.CurrentHtml);

        }


        private void sample_search()
        {
            var b = new Browser();
            b.Navigate("http://en.wikipedia.org");

            // Find for the form element to change.
            var searchInput = b.Find("searchInput");

            // Optionally, you could do some error checking to see if you found what you were looking for
            if (searchInput == null || searchInput.Exists == false)
            {
                throw new Exception("Element not found");
            }



            // Assign the value to the form element.
            searchInput.Value = "Mersenne twister";

            // Submit the form
            searchInput.SubmitForm();

            fwrite("result.html", b.CurrentHtml);

            //Console.WriteLine(b.CurrentHtml);
        }





        private void sample0() {

            var b = new Browser();
            b.Navigate("http://en.wikipedia.org");

            Console.WriteLine(b.Url);
            // http://en.wikipedia.org/wiki/Main_Page

            //Console.WriteLine(b.CurrentHtml);

            var todaysFeaturedArticle = b.Find("div", FindBy.Id, "mp-tfa");
            Console.WriteLine(todaysFeaturedArticle.Value);
            // Full text from the element and it's children. No Markup.

        }
        private void sample_()
        {
            var b = new Browser();
            b.Navigate("http://en.wikipedia.org");
            var links = b.Select("#mp-tfa a[href]"); // all links with a href inside #mp-tfa
            foreach (var link in links)
            {
                Console.WriteLine("Found link with text '{0}' and title '{1}' to {2}", link.Value, link.GetAttribute("title"), link.GetAttribute("href"));
            }
            var mainlink = b.Select("#mp-tfa b>a[href]");// all links with <a href> directly inside a <b> inside #mp-tfa
            mainlink.Click();
            Console.WriteLine("Url: {0}", b.Url);

            // Found link with text 'SMS Bayern' and title 'SMS Bayern' to /wiki/SMS_Bayern
            // Found link with text 'class' and title 'Ship class' to /wiki/Ship_class
            // Found link with text 'battleships' and title 'Battleship' to /wiki/Battleship
            // Found link with text 'German Imperial Navy' and title 'Kaiserliche Marine' to /wiki/Kaiserliche_Marine
            // ...
            // Url: http://en.wikipedia.org/wiki/SMS_Bayern
        }


        private void browse(string[] creds)
        {
            Browser browser = new Browser();
            try
            {
                // log the browser request/response data to files so we can interrogate them in case of an issue with our scraping
                browser.RequestLogged += OnBrowserRequestLogged;
                browser.MessageLogged += new Action<Browser, string>(OnBrowserMessageLogged);

                // we'll fake the user agent for websites that alter their content for unrecognised browsers
                browser.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.224 Safari/534.10";

                // browse to GitHub
                browser.Navigate("http://github.com/");
                if (LastRequestFailed(browser))
                {
                    return; // always check the last request in case the page failed to load
                }

                // click the login link and click it
                browser.Log("First we need to log in, so browse to the login page, fill in the login details and submit the form.");
                HtmlResult loginLink = browser.Find("a", FindBy.Text, "Sign in");
                if (!loginLink.Exists)
                {
                    browser.Log("Can't find the login link! Perhaps the site is down for maintenance?");
                }
                else
                {
                    loginLink.Click();
                    if (LastRequestFailed(browser))
                    {
                        return;
                    }

                    // fill in the form and click the login button - the fields are easy to locate because they have ID attributes
                    browser.Find("login_field").Value = creds[0];
                    browser.Find("password").Value = creds[1];
                    browser.Find(ElementType.Button, "name", "commit").Click();
                    if (LastRequestFailed(browser))
                    {
                        return;
                    }

                    // see if the login succeeded - ContainsText() is very forgiving, so don't worry about whitespace, casing, html tags separating the text, etc.
                    if (browser.ContainsText("Incorrect username or password"))
                    {
                        browser.Log("Login failed!", LogMessageType.Error);
                    }
                    else
                    {
                        // After logging in, we should check that the page contains elements that we recognise
                        if (!browser.ContainsText("Your Repositories"))
                        {
                            browser.Log("There wasn't the usual login failure message, but the text we normally expect isn't present on the page");
                        }
                        else
                        {
                            browser.Log("Your News Feed:");
                            // we can use simple jquery selectors, though advanced selectors are yet to be implemented
                            foreach (HtmlResult item in browser.Select("div.news .title"))
                            {
                                browser.Log("* " + item.Value);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                browser.Log(ex.Message, LogMessageType.Error);
                browser.Log(ex.StackTrace, LogMessageType.StackTrace);
            }
            finally
            {
                string path = WriteFile("log-" + DateTime.UtcNow.Ticks + ".html", browser.RenderHtmlLogFile("SimpleBrowser Sample - Request Log"));

            #if NETCOREAPP2_0
                            // dotnet core doesn't seem to be able to start process with path to html file
                            Console.WriteLine("Log file published to:");
                            Console.WriteLine(path);
            #else
                            Process.Start(path);
            #endif
            }
        }






        private static bool LastRequestFailed(Browser browser)
        {
            if (browser.LastWebException != null)
            {
                browser.Log("There was an error loading the page: " + browser.LastWebException.Message);
                return true;
            }
            return false;
        }

        private static void OnBrowserMessageLogged(Browser browser, string log)
        {
            Console.WriteLine(log);
        }

        private static void OnBrowserRequestLogged(Browser req, HttpRequestLog log)
        {
            Console.WriteLine(" -> " + log.Method + " request to " + log.Url);
            Console.WriteLine(" <- Response status code: " + log.ResponseCode);
        }

        private static string WriteFile(string filename, string text)
        {
            DirectoryInfo dir = new DirectoryInfo(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Logs"));
            if (!dir.Exists)
            {
                dir.Create();
            }

            string path = System.IO.Path.Combine(dir.FullName, filename);
            File.WriteAllText(path, text);
            return path;
        }










        //======================================
        //======================================
        //======================================
        //======================================
        //======================================
        //======================================
        //======================================
        private void pbargo_2()
        {
            //
            //MessageBox.Show("HI");

            //CreateDynamicProgressBarControl();


            for (int i = 1; i < 3; i++)
            {
                //pbar.Value++;
                pbar.Value = i;
                Thread.Sleep(500);
            }

            //ProgressBar progbar = new ProgressBar();
            //progbar.IsIndeterminate = false;
            //progbar.Orientation = Orientation.Horizontal;
            //progbar.Width = 150;
            //progbar.Height = 15;
            //Duration duration = new Duration(TimeSpan.FromSeconds(10));
            //DoubleAnimation doubleanimation = new DoubleAnimation(100.0, duration);
            //progbar.BeginAnimation(ProgressBar.ValueProperty, doubleanimation);




            //

            /*
                < BeginStoryboard >
                                < Storyboard >
                                    < DoubleAnimation
                Storyboard.TargetName = "progressBar1"
                Storyboard.TargetProperty = "Value"
                From = "0" To = "100" Duration = "0:0:5" />
    
                                    </ Storyboard >
    
                                </ BeginStoryboard >

                */
        }

        private void CreateDynamicProgressBarControl()
        {
            //https://www.c-sharpcorner.com/uploadfile/mahesh/wpf-progressbar/
            ProgressBar PBar2 = new ProgressBar();
            PBar2.IsIndeterminate = false;
            PBar2.Orientation = Orientation.Horizontal;
            PBar2.Width = 200;
            PBar2.Height = 20;
            Duration duration = new Duration(TimeSpan.FromSeconds(20));
            DoubleAnimation doubleanimation = new DoubleAnimation(200.0, duration);
            PBar2.BeginAnimation(ProgressBar.ValueProperty, doubleanimation);
            SBar.Items.Add(PBar2);
        }



        private void fwrite(string path, string content)
        {
            //https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/file-system/how-to-write-to-a-text-file
            File.WriteAllText(path, content);

            /*
            using (FileStream fs = File.Create(path))
            {
                // writing data in string
                string dataasstring = content; //your data
                byte[] info = new UTF8Encoding(true).GetBytes(dataasstring);
                fs.Write(info, 0, info.Length);

                // writing data in bytes already
                byte[] data = new byte[] { 0x0 };
                fs.Write(data, 0, data.Length);
            }
            */


        }

        private void IMap_Click(object sender, RoutedEventArgs e)
        {
            //var t = e.OriginalSource.ToString();
            //mailgo_thread();
            mailgo();
        }


        private void mailgo_thread()
        {
            //
            iMap.IsEnabled = false;

            Thread thread = new Thread(mailgo);
            thread.Start();


            iMap.IsEnabled = true;
            //
        }


        private void mailgo()
        {

            //imap lib

            //Mail.dll - https://www.nuget.org/packages/Mail.dll/
            //https://github.com/jstedfast/MailKit

            //AE.Net.Mail
            //https://www.nuget.org/packages/AE.Net.Mail/


            //string ffrom = "";
            //ffrom = "info@gigiena.pro";
            //ffrom = "tim.fmp@gmx.com";

            //https://yandex.ru/support/mail/mail-clients.html
            string host = "imap.yandex.ru", user = "timlance@yandex.ru", pass = "qwas55";
            host = "imap.yandex.ru"; user = "gamovamdcs@yandex.ru"; pass = "4828666";
            bool ssl = true;

            ushort port = 993;
            //AuthMethods method = AuthMethods.Login;

            string subject = "gigiena.pro";
            //string date = DateTime.Now.ToShortDateString();
            string date = DateTime.Now.ToString("yyyy-MM-dd");

            if (datepicker.SelectedDate != null)
            {
               //string tdate = datepicker.SelectedDate.Value.Date.ToShortDateString();

                //date = datepicker.SelectedDate.Value.ToShortDateString();

                date = datepicker.SelectedDate.Value.Year.ToString()
                    + "-" + datepicker.SelectedDate.Value.Month.ToString()
                    + "-" + datepicker.SelectedDate.Value.Day.ToString()
                    ;


            }

            //SelectedDate = "{Binding Path=DateOfBirth,StringFormat='yyyy-dd-MM'}"
            //"2019-03-26"
            //if (date != "")
            //{
            //    MessageBox.Show(date);
            //    return;
            //}

            //MessageBox.Show(date);
            //return;

            //IDictionary<int, string[]> result = aenetmail(host, user, pass, port, ssl, date, subject);
            IDictionary<string, string[]> result = MailKit(host, user, pass, port, ssl, date, subject);

            //IDictionary<string, string[]> result = new Dictionary<string, string[]>();

            //List<string[]> bulk = new List<string[]>();

            //ID: 40315793
            //ИМЯ: ТАТЬЯНА
            //ФАМИЛИЯ: НИКУЛИЧЕВА
            //ЛОГИН: 3be950d2
            //ПАРОЛЬ: EUaR##NV8Wpq
            //string[] item = { "3be950d2", "EUaR##NV8Wpq", "ID", "ИМЯ", "ФАМИЛИЯ" };

            //result["40315793"] = item;
            //bulk.Add(item);



            //26.03.2019 32247696, СВЕТЛАНА, СОЛДАТОВА, 288fe0f8, 8nJMzN4R - eB!, 
            string report = "";

            //return;
            string url = "https://gigiena.pro/auth/lencamp/login.php";
            //samplego(url, bulk);
            IDictionary<string, string[]> result2 = browse(url, result);


            foreach (var res in result2)
            {
                //string[] item = { res.Value[3], res.Value[4] };

                //bulk.Add(item);


                report += res.Key + " - " +
                      res.Value[0] + " , "
                    + res.Value[1] + " , "
                    + res.Value[2] + " , "
                    + res.Value[3] + " , "
                    + res.Value[4] + "  "

                    + "\n\r";
                //report += res.Key + " - " +  res.Value[0] + ";";
            }



            fwrite("data.html", report);

            //string exePath = GetApplicationRoot() + @"\";

            fdirEnforce("tests");
            fwrite("tests/testing " + date + ".txt", report);

            MessageBox.Show("OK");
            //tfresult.Text = report;

        }




        /* */
        private int MailKitSearch(string host, string user,
            string pass, ushort port, bool secure,
                 string date, string subject)
        {//https://github.com/jstedfast/MailKit

            IDictionary<string, string[]> result = new Dictionary<string, string[]>();

            int mcount = 0;

            try
            {
                //"imap.gmail.com", "name@gmail.com", "pass", null, 993, true
                //string host, string username, string password, 
                //AuthMethods method = AuthMethods.Login, int port = 143, bool secure = false, bool skipSslValidation = false




                var client = new ImapClient();
                // For demo-purposes, accept all SSL certificates
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                //string host = "imap.yandex.ru", user = "timlance@yandex.ru", pass = "qwas55";
                //host = "imap.yandex.ru"; user = "gamovamdcs@yandex.ru"; pass = "4828666";

                client.Connect(host, 993, true);
                client.Authenticate(user, pass);

                // The Inbox folder is always available on all IMAP servers...
                var inbox = client.Inbox;
                //inbox.Open(FolderAccess.ReadOnly);
                inbox.Open(FolderAccess.ReadWrite);

                //Console.WriteLine("Total messages: {0}", inbox.Count);
                //Console.WriteLine("Recent messages: {0}", inbox.Recent);




                //var query = SearchQuery.DeliveredAfter(DateTime.Parse("2019-03-26"))
                var query = SearchQuery
                  //  .DeliveredOn(DateTime.Parse(date))
                  //.And(SearchQuery.SubjectContains(subject))
                  ////.And(SearchQuery.Seen)
                  .SubjectContains(subject)
                  ;

                IList<UniqueId> msearch = inbox.Search(query);
                
                mcount = msearch.Count;
                Console.WriteLine("Total count: {0}", mcount);

                //create folder
                //var personal = client.GetFolder(client.PersonalNamespaces[0]); personal.Create("stuff", true)ж



                string farchive = "test_archive";
                var archf = client.GetFolder(farchive);
                archf.Open(FolderAccess.ReadWrite);
                inbox.Open(FolderAccess.ReadWrite);
                //
                foreach (var uid in msearch)
                {
                    var message = inbox.GetMessage(uid);

                    if (archf != null)
                    {
                        inbox.MoveTo(uid, archf);
                        //archf.Append(message);
                    }

                }


                archf.Expunge();//flag moving messages


            }
            catch (System.Net.Sockets.SocketException exc)
            {

                string err = exc.ToString();

                MessageBox.Show("No connection\n\r" + err, "System.Net.Sockets.SocketException");

            }



            return mcount;

        }//




         private IDictionary<string, string[]> MailKit(string host, string user, string pass, ushort port, bool secure,
            string date, string subject)
        {//https://github.com/jstedfast/MailKit

            IDictionary<string, string[]> result = new Dictionary<string, string[]>();

            string farchive = "test_archive";

            try
            {
                //"imap.gmail.com", "name@gmail.com", "pass", null, 993, true
                //string host, string username, string password, 
                //AuthMethods method = AuthMethods.Login, int port = 143, bool secure = false, bool skipSslValidation = false

              


                var client = new ImapClient();
                // For demo-purposes, accept all SSL certificates
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                //string host = "imap.yandex.ru", user = "timlance@yandex.ru", pass = "qwas55";
                //host = "imap.yandex.ru"; user = "gamovamdcs@yandex.ru"; pass = "4828666";

                client.Connect(host, 993, true);
                client.Authenticate(user, pass);

                // The Inbox folder is always available on all IMAP servers...
                var inbox = client.Inbox;
                inbox.Open(FolderAccess.ReadOnly);

                //Console.WriteLine("Total messages: {0}", inbox.Count);
                //Console.WriteLine("Recent messages: {0}", inbox.Recent);

                string report = "";


                //var query = SearchQuery.DeliveredAfter(DateTime.Parse("2019-03-26"))
                var query = SearchQuery.DeliveredOn(DateTime.Parse(date))
                  .And(SearchQuery.SubjectContains(subject))
                  //.And(SearchQuery.Seen)
                  ;

                string[] breakers = {


                            "ID: ",
                            "ИМЯ: ",
                            "ФАМИЛИЯ: ",
                            "ЛОГИН: ",
                            "ПАРОЛЬ: ",
                            "ДЛЯ ПРОХОЖДЕНИЯ"


                        };
                int i = 0;


                foreach (var uid in inbox.Search(query))
                {
                    var message = inbox.GetMessage(uid);
                    string[] sdate = message.Date.ToString().Split(' ');

                    //string[] pcs = parse_plain(WebUtility.HtmlDecode(html2text(message.HtmlBody)), breakers);
                    string[] pcs = parse_plain((html2text(message.HtmlBody)), breakers);

                    string[] item = {

                        //DecodeEncodedNonAsciiCharacters( message.Subject) + " " -
                            //uid.ToString().Trim() + " " + 
                            
                            //sdate[0] + " " +
                            //+ WebUtility.HtmlDecode (html2text(letter.Body)) 
                            //m.Date.ToShortDateString()
                            //+ pcs[0]
                             pcs[4].Trim()
                           , pcs[5].Trim()

                           , pcs[1].Trim()
                           , pcs[2].Trim()
                           , pcs[3].Trim()
                         
                            //+ (WebUtility.HtmlDecode (html2text(letter.Body))).Replace("ID", "@#TADA")
                            };


                    //IDictionary<string, string> item = new Dictionary<string, string>();
                    //item["subject"] = m.Subject;
                    //item["date"] = m.Date.ToLongDateString();

                    result.Add(pcs[1], item);
                    //result[pcs[1]] = (item);
                    i++;

                    //Console.WriteLine("[match] {0}: {1}", pcs[4], pcs[5]);

                    report += pcs[4].Trim()
                           + " " + pcs[5].Trim()

                           + " " + pcs[1].Trim()
                           + " " + pcs[2].Trim()
                           + " " + pcs[3].Trim()
                           ;


                    //message.move to archive





                }//foreach message




                tfresult.Text = report;


                client.Disconnect(true);


            }
            catch (System.Net.Sockets.SocketException exc)
            {

                string err = exc.ToString();

                MessageBox.Show("No connection\n\r" + err, "System.Net.Sockets.SocketException");

            }


            return result;
        }//


        /*

        private IDictionary<int, string[]> aenetmail(string host, string user, string pass, ushort port, bool secure,
            string date, string subject)
        {
            IDictionary<int, string[]> result = new Dictionary<int, string[]>();

            AuthMethods method = AuthMethods.Login;

            try
            { 
            //"imap.gmail.com", "name@gmail.com", "pass", null, 993, true
            //string host, string username, string password, 
            //AuthMethods method = AuthMethods.Login, int port = 143, bool secure = false, bool skipSslValidation = false

            ImapClient ic = new ImapClient(host, user, pass, method, port, secure);
            // Select a mailbox. Case-insensitive
            ic.SelectMailbox("INBOX");
                int total = (ic.GetMessageCount());

                SearchCondition condition = new SearchCondition();
                condition.Field = SearchCondition.Fields.Since;
                condition.Value = new DateTime(2019, 3, 12).ToString("dd-MMM-yyyy");

                //var mm = ic.SearchMessages(condition);

                MailMessage[] mm = ic.GetMessages(0, 300 - 1, true);
                //MailMessage[] mm = ic.GetMessages(0, ic.GetMessageCount()  );
                //MailMessage[] mm = ic.GetMessages(2000, ic.GetMessageCount());
                int i = 0;


                if (1 == 11)// for i/foreach
                {

                    for (int j = total; j > 0; j--)
                    {
                        //if (date != m.Date.ToString()) continue;
                        if (!mm[j].Subject.Contains(subject)) continue;

                        //string[] item = new string[3];
                        MailMessage letter = ic.GetMessage(mm[j].Uid.Trim());

                        string[] item = { DecodeEncodedNonAsciiCharacters( mm[j].Subject) + " "
                            + mm[j].Uid.Trim() + " " + mm[j].Date.ToShortDateString() +
                             WebUtility.HtmlDecode(html2text(letter.Body))
                            };
                        //IDictionary<string, string> item = new Dictionary<string, string>();
                        //item["subject"] = m.Subject;
                        //item["date"] = m.Date.ToLongDateString();

                        result.Add(i, item);
                        i++;

                        //Console.WriteLine(m.Subject);
                        //Console.WriteLine(m.Date);
                    }



                } else// for i/foreach
                {

                    foreach (MailMessage m in mm)

                    {
                        //if (date != m.Date.ToString()) continue;
                        if (!m.Subject.Contains(subject)) continue;

                        //string[] item = new string[3];
                        MailMessage letter = ic.GetMessage(m.Uid.Trim());

                        string[] breakers = {


                            "ID: ",
                            "ИМЯ: ",
                            "ФАМИЛИЯ: ",
                            "ЛОГИН: ",
                            "ПАРОЛЬ: ",
                            "ДЛЯ ПРОХОЖДЕНИЯ"

                            // "ID",
                            // "ИМЯ",
                            //"ФАМИЛИЯ",
                            //"ЛОГИН",
                            //"ПАРОЛЬ",
                            //"ДЛЯ ПРОХОЖДЕНИЯ"

                        };

                        string[] pcs = parse_plain(WebUtility.HtmlDecode(html2text(letter.Body)), breakers);

                        string[] item = { DecodeEncodedNonAsciiCharacters( m.Subject) + " "
                            + m.Uid.Trim() + " " + m.Date.ToShortDateString() + " "
                            //+ WebUtility.HtmlDecode (html2text(letter.Body))
                            + pcs[0]
                            + pcs[1]
                            + pcs[2]
                            + pcs[3]
                            + pcs[4]
                            + pcs[5]
                            //+ (WebUtility.HtmlDecode (html2text(letter.Body))).Replace("ID", "@#TADA")
                            };
                        //IDictionary<string, string> item = new Dictionary<string, string>();
                        //item["subject"] = m.Subject;
                        //item["date"] = m.Date.ToLongDateString();

                        result.Add(i, item);
                        i++;

                        //Console.WriteLine(m.Subject);
                        //Console.WriteLine(m.Date);
                    }

                }//toggle loop type



            // Probably wiser to use a using statement
            ic.Dispose();

        }catch(System.Net.Sockets.SocketException exc){

                string err = exc.ToString();

                MessageBox.Show("No connection\n\r" + err , "System.Net.Sockets.SocketException");
        
        }


            return result;
        }//
        */


        private void fdirEnforce(string target)
        {
            if (!Directory.Exists(target))
            {
                Directory.CreateDirectory(target);
            }
        }

        public string[] parse_plain(string text, string[] items)
        {
            //IDictionary<string, string> result = new Dictionary<string, string>();
            string splitter = "@#";

            foreach (string pc in items)
            {
                text = text.Replace(pc, splitter);
            }

            string[] contpcs = Regex.Split(text, splitter);

            return contpcs;
        }




        public string Convert(string str)
        {
            byte[] utf8Bytes = Encoding.UTF8.GetBytes(str);
            //str = Encoding.UTF8.GetString(utf8Bytes);
            str = Encoding.Unicode.GetString(utf8Bytes);


            return str;
        }

        private static string html2text(string text)
        {
            text = Regex.Replace(text, "<[^>]*(>|$)", "",
                                      RegexOptions.IgnoreCase,
                                      TimeSpan.FromSeconds(0.5));

            return text;
        }



        public static string EncodeNonAsciiCharacters(string value)
        {
            return Regex.Replace(
              value,
              @"[^\x00-\x7F]",
              m => String.Format("\\u{0:X4}", (int)m.Value[0]));
        }

        public string DecodeEncodedNonAsciiCharacters(string value)
        {
            return Regex.Replace(
                value,
                @"\\u(?<Value>[a-zA-Z0-9]{4})",
                m =>
                {
                    return ((char)int.Parse(m.Groups["Value"].Value, NumberStyles.HexNumber)).ToString();
                });
        }


        public string HTMLEncodeSpecialChars(string text)
        {
            //HttpUtility.HtmlEncode()
            //HttpUtility.HtmlDecode()

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (text != null)
            {
                foreach (char c in text)
                {
                    //if (c > 127) // special chars ,  || c == 60 || c == 62
                    if (!Char.IsLetterOrDigit(c)) // special chars
                        sb.Append(String.Format("&#{0};", (int)c));
                    else
                        sb.Append(c);
                }
            }
            return sb.ToString();


            //a reverse function:
            //string encodedString = "&#1040;&#1112;&#1072;";
            //string[] splittedChars;
            //string decodedString = "";
            //splittedChars = endodedString.Split(new char[] { ';' });
            //foreach (string s in splittedChars)
            //{
            //    decodedString += (char)(Convert.ToInt32(s.Replace("&#", "")));
            //}
        }



        public string win2utf8(string source)
        {
            //https://stackoverflow.com/questions/42884025/c-sharp-encode-connection-string-from-win1251-to-utf8-and-back
            string result = "";

            Encoding utf8 = Encoding.UTF8;
            Encoding unicode = Encoding.Unicode;
            Encoding win1251 = Encoding.GetEncoding("windows-1251");
            byte[] utf8Bytes = utf8.GetBytes(source);
            byte[] win1251Bytes = win1251.GetBytes(source);
            byte[] utf8ofwinBytes = Encoding.Convert(win1251, utf8, win1251Bytes);
            string unicodefromutf8 = utf8.GetString(utf8Bytes);
            string unicodefromwin1251 = win1251.GetString(win1251Bytes);

            result = unicodefromutf8;

            //Encoding utf8 = Encoding.UTF8;
            //Encoding win = Encoding.GetEncoding("windows-1251");
            //byte[] winBytes = win.GetBytes(source);
            //byte[] utfBytes = Encoding.Convert(win, utf8, winBytes);
            //string result = utf8.GetString(utfBytes);



            return result;
        }


        // Function to detect the encoding for UTF-7, UTF-8/16/32 (bom, no bom, little
        // & big endian), and local default codepage, and potentially other codepages.
        // 'taster' = number of bytes to check of the file (to save processing). Higher
        // value is slower, but more reliable (especially UTF-8 with special characters
        // later on may appear to be ASCII initially). If taster = 0, then taster
        // becomes the length of the file (for maximum reliability). 'text' is simply
        // the string with the discovered encoding applied to the file.
        public Encoding detectTextEncoding(string filename, out String text, int taster = 1000)
        {
            byte[] b = File.ReadAllBytes(filename);

            //////////////// First check the low hanging fruit by checking if a
            //////////////// BOM/signature exists (sourced from http://www.unicode.org/faq/utf_bom.html#bom4)
            if (b.Length >= 4 && b[0] == 0x00 && b[1] == 0x00 && b[2] == 0xFE && b[3] == 0xFF) { text = Encoding.GetEncoding("utf-32BE").GetString(b, 4, b.Length - 4); return Encoding.GetEncoding("utf-32BE"); }  // UTF-32, big-endian 
            else if (b.Length >= 4 && b[0] == 0xFF && b[1] == 0xFE && b[2] == 0x00 && b[3] == 0x00) { text = Encoding.UTF32.GetString(b, 4, b.Length - 4); return Encoding.UTF32; }    // UTF-32, little-endian
            else if (b.Length >= 2 && b[0] == 0xFE && b[1] == 0xFF) { text = Encoding.BigEndianUnicode.GetString(b, 2, b.Length - 2); return Encoding.BigEndianUnicode; }     // UTF-16, big-endian
            else if (b.Length >= 2 && b[0] == 0xFF && b[1] == 0xFE) { text = Encoding.Unicode.GetString(b, 2, b.Length - 2); return Encoding.Unicode; }              // UTF-16, little-endian
            else if (b.Length >= 3 && b[0] == 0xEF && b[1] == 0xBB && b[2] == 0xBF) { text = Encoding.UTF8.GetString(b, 3, b.Length - 3); return Encoding.UTF8; } // UTF-8
            else if (b.Length >= 3 && b[0] == 0x2b && b[1] == 0x2f && b[2] == 0x76) { text = Encoding.UTF7.GetString(b, 3, b.Length - 3); return Encoding.UTF7; } // UTF-7


            //////////// If the code reaches here, no BOM/signature was found, so now
            //////////// we need to 'taste' the file to see if can manually discover
            //////////// the encoding. A high taster value is desired for UTF-8
            if (taster == 0 || taster > b.Length) taster = b.Length;    // Taster size can't be bigger than the filesize obviously.


            // Some text files are encoded in UTF8, but have no BOM/signature. Hence
            // the below manually checks for a UTF8 pattern. This code is based off
            // the top answer at: https://stackoverflow.com/questions/6555015/check-for-invalid-utf8
            // For our purposes, an unnecessarily strict (and terser/slower)
            // implementation is shown at: https://stackoverflow.com/questions/1031645/how-to-detect-utf-8-in-plain-c
            // For the below, false positives should be exceedingly rare (and would
            // be either slightly malformed UTF-8 (which would suit our purposes
            // anyway) or 8-bit extended ASCII/UTF-16/32 at a vanishingly long shot).
            int i = 0;
            bool utf8 = false;
            while (i < taster - 4)
            {
                if (b[i] <= 0x7F) { i += 1; continue; }     // If all characters are below 0x80, then it is valid UTF8, but UTF8 is not 'required' (and therefore the text is more desirable to be treated as the default codepage of the computer). Hence, there's no "utf8 = true;" code unlike the next three checks.
                if (b[i] >= 0xC2 && b[i] <= 0xDF && b[i + 1] >= 0x80 && b[i + 1] < 0xC0) { i += 2; utf8 = true; continue; }
                if (b[i] >= 0xE0 && b[i] <= 0xF0 && b[i + 1] >= 0x80 && b[i + 1] < 0xC0 && b[i + 2] >= 0x80 && b[i + 2] < 0xC0) { i += 3; utf8 = true; continue; }
                if (b[i] >= 0xF0 && b[i] <= 0xF4 && b[i + 1] >= 0x80 && b[i + 1] < 0xC0 && b[i + 2] >= 0x80 && b[i + 2] < 0xC0 && b[i + 3] >= 0x80 && b[i + 3] < 0xC0) { i += 4; utf8 = true; continue; }
                utf8 = false; break;
            }
            if (utf8 == true)
            {
                text = Encoding.UTF8.GetString(b);
                return Encoding.UTF8;
            }


            // The next check is a heuristic attempt to detect UTF-16 without a BOM.
            // We simply look for zeroes in odd or even byte places, and if a certain
            // threshold is reached, the code is 'probably' UF-16.          
            double threshold = 0.1; // proportion of chars step 2 which must be zeroed to be diagnosed as utf-16. 0.1 = 10%
            int count = 0;
            for (int n = 0; n < taster; n += 2) if (b[n] == 0) count++;
            if (((double)count) / taster > threshold) { text = Encoding.BigEndianUnicode.GetString(b); return Encoding.BigEndianUnicode; }
            count = 0;
            for (int n = 1; n < taster; n += 2) if (b[n] == 0) count++;
            if (((double)count) / taster > threshold) { text = Encoding.Unicode.GetString(b); return Encoding.Unicode; } // (little-endian)


            // Finally, a long shot - let's see if we can find "charset=xyz" or
            // "encoding=xyz" to identify the encoding:
            for (int n = 0; n < taster - 9; n++)
            {
                if (
                    ((b[n + 0] == 'c' || b[n + 0] == 'C') && (b[n + 1] == 'h' || b[n + 1] == 'H') && (b[n + 2] == 'a' || b[n + 2] == 'A') && (b[n + 3] == 'r' || b[n + 3] == 'R') && (b[n + 4] == 's' || b[n + 4] == 'S') && (b[n + 5] == 'e' || b[n + 5] == 'E') && (b[n + 6] == 't' || b[n + 6] == 'T') && (b[n + 7] == '=')) ||
                    ((b[n + 0] == 'e' || b[n + 0] == 'E') && (b[n + 1] == 'n' || b[n + 1] == 'N') && (b[n + 2] == 'c' || b[n + 2] == 'C') && (b[n + 3] == 'o' || b[n + 3] == 'O') && (b[n + 4] == 'd' || b[n + 4] == 'D') && (b[n + 5] == 'i' || b[n + 5] == 'I') && (b[n + 6] == 'n' || b[n + 6] == 'N') && (b[n + 7] == 'g' || b[n + 7] == 'G') && (b[n + 8] == '='))
                    )
                {
                    if (b[n + 0] == 'c' || b[n + 0] == 'C') n += 8; else n += 9;
                    if (b[n] == '"' || b[n] == '\'') n++;
                    int oldn = n;
                    while (n < taster && (b[n] == '_' || b[n] == '-' || (b[n] >= '0' && b[n] <= '9') || (b[n] >= 'a' && b[n] <= 'z') || (b[n] >= 'A' && b[n] <= 'Z')))
                    { n++; }
                    byte[] nb = new byte[n - oldn];
                    Array.Copy(b, oldn, nb, 0, n - oldn);
                    try
                    {
                        string internalEnc = Encoding.ASCII.GetString(nb);
                        text = Encoding.GetEncoding(internalEnc).GetString(b);
                        return Encoding.GetEncoding(internalEnc);
                    }
                    catch { break; }    // If C# doesn't recognize the name of the encoding, break.
                }
            }


            // If all else fails, the encoding is probably (though certainly not
            // definitely) the user's local codepage! One might present to the user a
            // list of alternative encodings as shown here: https://stackoverflow.com/questions/8509339/what-is-the-most-common-encoding-of-each-language
            // A full list can be found using Encoding.GetEncodings();
            text = Encoding.Default.GetString(b);
            return Encoding.Default;
        }//


        private void aenetSearch()
        {
            /*

            using (var imap = new AE.Net.Mail.ImapClient(host, user, pass, method, port, ssl))
            {
                var msgs = imap.SearchMessages(
                  SearchCondition.Undeleted().And(
                    SearchCondition.From(ffrom),
                    SearchCondition.SentSince(new DateTime(2019, 3, 25))
                  )
                //.Or(SearchCondition.To("andy"))
                );

                //Assert.AreEqual(msgs[0].Value.Subject, "This is cool!");

                //IDictionary<int, string[]> result = new Dictionary<int, string[]>();
                List<string[]> result = new List<string[]>();

                if (msgs == null) Console.WriteLine("NO msgs");

                foreach (var m in msgs)
                {
                    IDictionary<string, string> item = new Dictionary<string, string>();
                    item["subject"] = m.ToString();
                    //item["date"] = m.Date.ToLongDateString();

                    //result.Add(item);

                    Console.WriteLine(m.ToString());

                }


            }//

            */
        }

        private void Button_Click_1(object sender, RoutedEventArgs ee)
        {

                var client = new ImapClient();
                // For demo-purposes, accept all SSL certificates
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                string host = "imap.yandex.ru", user = "timlance@yandex.ru", pass = "qwas55";
                host = "imap.yandex.ru"; user = "gamovamdcs@yandex.ru"; pass = "4828666";

                client.Connect(host, 993, true);
                

                client.Authenticate(user, pass);

                // The Inbox folder is always available on all IMAP servers...
                var inbox = client.Inbox;
                inbox.Open(FolderAccess.ReadOnly);

                Console.WriteLine("Total messages: {0}", inbox.Count);
                Console.WriteLine("Recent messages: {0}", inbox.Recent);

                string report = "";


                //var query = SearchQuery.DeliveredAfter(DateTime.Parse("2019-03-26"))
                var query = SearchQuery.DeliveredOn(DateTime.Parse("2019-03-26"))
                  .And(SearchQuery.SubjectContains("gigiena.pro"))
                  //.And(SearchQuery.Seen)
                  ;

                foreach (var uid in inbox.Search(query))
                {
                    var message = inbox.GetMessage(uid);
                    Console.WriteLine("[match] {0}: {1}", uid, message.Subject);
                }




                tfresult.Text = report;


                client.Disconnect(true);
            
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            //datepicker.ClearValue(datepicker.SelectedDate.Value);
            //datepicker.SelectedDate.Value = null;

            datepicker.SelectedDate = null;
            datepicker.DisplayDate = DateTime.Today;

        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            string host = "imap.yandex.ru", user = "timlance@yandex.ru", pass = "qwas55";
            host = "imap.yandex.ru"; user = "gamovamdcs@yandex.ru"; pass = "4828666";
            bool ssl = true;

            ushort port = 993;
            //AuthMethods method = AuthMethods.Login;

            string subject = "gigiena.pro";
            //string date = DateTime.Now.ToShortDateString();
            string date = DateTime.Now.ToString("yyyy-MM-dd");


            MailKitSearch(host, user, pass, port, ssl, date, subject);
        }
    }







}
